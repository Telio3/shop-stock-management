package com.example.webservice.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name="articles")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column()
    private String designation;
    private Integer quantity;
    private Float price;

    @OneToMany(mappedBy = "article")
    private List<OrderArticle> orderArticles;
}
