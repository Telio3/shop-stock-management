package com.example.webservice.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column()
    private Date date;

    @PrePersist
    private void prePersist() {
        date = new Date();
    }

    @Column()
    private Float totalPrice;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderArticle> orderArticles = new ArrayList<>();

    public void setOrderArticles(List<OrderArticle> orderArticles) {
        this.orderArticles.clear();
        if (orderArticles != null) {
            this.orderArticles.addAll(orderArticles);
            for (OrderArticle orderArticle : orderArticles) {
                orderArticle.setOrder(this);
            }
        }
    }

}
