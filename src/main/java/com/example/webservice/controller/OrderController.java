package com.example.webservice.controller;

import com.example.webservice.controller.dto.OrderDto;
import com.example.webservice.model.Article;
import com.example.webservice.model.ArticleQuantity;
import com.example.webservice.model.Order;
import com.example.webservice.service.ArticleService;
import com.example.webservice.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Tag(name = "Order")
@AllArgsConstructor
@RestController
@RequestMapping(
        path = "/orders",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class OrderController {
    private final OrderService orderService;
    private final ArticleService articleService;

    @Operation(summary = "Get all orders")
    @GetMapping
    public ResponseEntity<List<OrderDto>> getAll() {
        return ResponseEntity.ok(this.orderService.getAll()
                .stream()
                .map(this::mapToDto)
                .collect(Collectors.toList()));
    }

    @Operation(summary = "Get order by id")
    @GetMapping(path = "{id}")
    public ResponseEntity<OrderDto> getById(@PathVariable Long id) {
        return this.orderService.getById(id)
                .map(order -> ResponseEntity.ok(mapToDto(order)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Delete order by id")
    @DeleteMapping(path = "{id}")
    public ResponseEntity<Long> deleteById(@PathVariable Long id) {
        try {
            this.orderService.deleteById(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Create order")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDto> create(@RequestBody OrderDto orderDto) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(mapToDto(this.orderService.create(orderDto.getArticles())));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Operation(summary = "Update order")
    @PutMapping(path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDto> update(@PathVariable Long id, @RequestBody OrderDto orderDto) {
        try {
            return ResponseEntity.ok().body(mapToDto(this.orderService.update(id, orderDto.getArticles())));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.badRequest().build();
        }
    }

    private OrderDto mapToDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setDate(order.getDate());
        orderDto.setTotalPrice(order.getTotalPrice());
        orderDto.setArticles(order.getOrderArticles()
                .stream()
                .map(orderArticle -> {
                    ArticleQuantity articleQuantity = new ArticleQuantity();
                    articleQuantity.setArticleId(orderArticle.getArticle().getId());
                    articleQuantity.setQuantity(orderArticle.getQuantity());

                    return articleQuantity;
                })
                .collect(Collectors.toList()));

        return orderDto;
    }
}
