package com.example.webservice.controller.dto;

import com.example.webservice.model.ArticleQuantity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
    private Long id;
    private Date date;
    private Float totalPrice;
    private List<ArticleQuantity> articles;
}
