package com.example.webservice.controller;

import com.example.webservice.controller.dto.ArticleDto;
import com.example.webservice.model.Article;
import com.example.webservice.service.ArticleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Article")
@AllArgsConstructor
@RestController
@RequestMapping(
        path = "/articles",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class ArticleController {
    private final ArticleService articleService;

    @Operation(summary = "Get all articles")
    @GetMapping
    public ResponseEntity<List<ArticleDto>> getAll() {
        return ResponseEntity.ok(this.articleService.getAll()
                .stream()
                .map(this::mapToDto)
                .collect(Collectors.toList()));
    }

    @Operation(summary = "Get an article by its id")
    @GetMapping(path = "{id}")
    public ResponseEntity<ArticleDto> getById(@PathVariable Long id) {
        return this.articleService.getById(id)
                .map(article -> ResponseEntity.ok(mapToDto(article)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Delete an article by its id")
    @DeleteMapping(path = "{id}")
    public ResponseEntity<Long> deleteById(@PathVariable Long id) {
        try {
            this.articleService.deleteById(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Allows you to create an article")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArticleDto> create(@RequestBody ArticleDto articleDto) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(mapToDto(this.articleService.create(articleDto.getDesignation(), articleDto.getQuantity(), articleDto.getPrice())));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Operation(summary = "Allows you to update an article")
    @PutMapping(path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArticleDto> update(@PathVariable Long id, @RequestBody ArticleDto articleDto) {
        try {
            return ResponseEntity.ok().body(mapToDto(this.articleService.update(id, articleDto.getDesignation(), articleDto.getQuantity(), articleDto.getPrice())));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    private ArticleDto mapToDto(Article article) {
        ArticleDto articleDto = new ArticleDto();
        articleDto.setId(article.getId());
        articleDto.setDesignation(article.getDesignation());
        articleDto.setQuantity(article.getQuantity());
        articleDto.setPrice(article.getPrice());

        return articleDto;
    }
}
