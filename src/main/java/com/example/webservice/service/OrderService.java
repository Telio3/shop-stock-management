package com.example.webservice.service;

import com.example.webservice.model.ArticleQuantity;
import com.example.webservice.model.Order;
import com.example.webservice.model.OrderArticle;
import com.example.webservice.model.Article;
import com.example.webservice.repository.ArticleRepository;
import com.example.webservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final ArticleRepository articleRepository;

    public List<Order> getAll() {
        return this.orderRepository.findAll();
    }

    public Optional<Order> getById(Long id) {
        return this.orderRepository.findById(id);
    }

    public void deleteById(Long id) throws Exception {
        Optional<Order> optionalOrder = orderRepository.findById(id);

        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();

            // Restaurer la quantité des articles dans le stock
            for (OrderArticle orderArticle : order.getOrderArticles()) {
                Article article = orderArticle.getArticle();
                int quantity = orderArticle.getQuantity();
                article.setQuantity(article.getQuantity() + quantity);

                articleRepository.save(article);
            }

            orderRepository.deleteById(id);
        } else {
            throw new Exception("Id not found");
        }
    }

    public Order create(List<ArticleQuantity> articleQuantities) {
        Order order = new Order();
        order.setId(null);
        order.setDate(null);

        List<OrderArticle> orderArticles = new ArrayList<>();
        float totalPrice = 0; // Variable pour calculer le prix total

        for (ArticleQuantity articleQuantity : articleQuantities) {
            Optional<Article> optionalArticle = articleRepository.findById(articleQuantity.getArticleId());

            if (optionalArticle.isPresent()) {
                Article article = optionalArticle.get();

                int quantity = articleQuantity.getQuantity();

                // Vérifier si la quantité demandée est disponible dans le stock
                if (article.getQuantity() >= quantity) {
                    // Mettre à jour la quantité de l'article dans le stock
                    article.setQuantity(article.getQuantity() - quantity);

                    OrderArticle orderArticle = new OrderArticle();
                    orderArticle.setArticle(article);
                    orderArticle.setQuantity(quantity);
                    orderArticle.setOrder(order);
                    orderArticles.add(orderArticle);

                    // Ajouter le prix de l'article multiplié par la quantité au prix total
                    totalPrice += article.getPrice() * quantity;
                } else {
                    throw new IllegalArgumentException("Insufficient quantity for article: " + article.getId());
                }
            }
        }

        order.setOrderArticles(orderArticles);
        order.setTotalPrice(totalPrice); // Définir le prix total de la commande

        return orderRepository.save(order);
    }

    public Order update(Long id, List<ArticleQuantity> articleQuantities) throws Exception {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();

            // Restaurer la quantité des articles précédents dans le stock
            restorePreviousArticleQuantities(order);

            List<OrderArticle> orderArticles = new ArrayList<>();
            float totalPrice = 0; // Variable pour calculer le prix total

            for (ArticleQuantity articleQuantity : articleQuantities) {
                Article article = articleRepository.findById(articleQuantity.getArticleId()).get();
                int quantity = articleQuantity.getQuantity();

                // Vérifier si la quantité demandée est disponible dans le stock
                if (article.getQuantity() >= quantity) {
                    // Mettre à jour la quantité de l'article dans le stock
                    article.setQuantity(article.getQuantity() - quantity);

                    OrderArticle orderArticle = new OrderArticle();
                    orderArticle.setArticle(article);
                    orderArticle.setQuantity(quantity);
                    orderArticle.setOrder(order);
                    orderArticles.add(orderArticle);

                    // Ajouter le prix de l'article multiplié par la quantité au prix total
                    totalPrice += article.getPrice() * quantity;
                } else {
                    throw new IllegalArgumentException("Insufficient quantity for article: " + article.getId());
                }
            }

            order.setOrderArticles(orderArticles);
            order.setTotalPrice(totalPrice); // Mettre à jour le prix total de la commande

            return orderRepository.save(order);
        } else {
            throw new Exception("Order not found");
        }
    }

    private void restorePreviousArticleQuantities(Order order) {
        List<OrderArticle> orderArticles = order.getOrderArticles();

        for (OrderArticle orderArticle : orderArticles) {
            Article article = orderArticle.getArticle();
            int quantity = orderArticle.getQuantity();
            article.setQuantity(article.getQuantity() + quantity);
        }
    }
}
