package com.example.webservice.service;

import com.example.webservice.model.Article;
import com.example.webservice.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ArticleService {
    private final ArticleRepository articleRepository;

    public List<Article> getAll() {
        return this.articleRepository.findAll();
    }

    public Optional<Article> getById(Long id) {
        return this.articleRepository.findById(id);
    }

    public void deleteById(Long id) throws Exception {
        if(this.articleRepository.findById(id).isPresent()) {
            this.articleRepository.deleteById(id);
        } else {
            throw new Exception("Id not found");
        }
    }

    public Article create(String designation, int quantity, Float price) {
        Article article = new Article();
        article.setId(null);
        article.setDesignation(designation);
        article.setQuantity(quantity);
        article.setPrice(price);

        return this.articleRepository.save(article);
    }

    public Article update(Long id, String designation, int quantity, Float price) throws Exception {
        if(this.articleRepository.findById(id).isEmpty()) {
            throw new Exception("ID not found");
        }

        Article articleToUpdate = this.articleRepository.findById(id).orElseThrow();
        if (designation != null) articleToUpdate.setDesignation(designation);
        if (quantity != 0) articleToUpdate.setQuantity(quantity);
        if (price != null) articleToUpdate.setPrice(price);

        return this.articleRepository.save(articleToUpdate);
    }
}
