package com.example.webservice.service;

import com.example.webservice.model.Article;
import com.example.webservice.repository.ArticleRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class ArticleServiceTest {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleRepository articleRepository;

    @Test
    void getAllArticlesTest() {
        // Création de plusieurs articles
        Article article1 = new Article();
        article1.setDesignation("Article 1");
        article1.setQuantity(5);
        article1.setPrice(19.99f);

        Article article2 = new Article();
        article2.setDesignation("Article 2");
        article2.setQuantity(3);
        article2.setPrice(14.99f);

        Article article3 = new Article();
        article3.setDesignation("Article 3");
        article3.setQuantity(8);
        article3.setPrice(9.99f);

        // Enregistrement des articles dans la base de données
        articleRepository.saveAll(List.of(article1, article2, article3));

        // Récupération de tous les articles
        List<Article> articles = articleService.getAll();

        // Vérification du nombre d'articles récupérés
        assertEquals(3, articles.size());
    }

    @Test
    void getArticleByIdTest() {
        // Création d'un article
        Article article = new Article();
        article.setDesignation("Nom de l'article");
        article.setQuantity(10);
        article.setPrice(9.99f);

        // Enregistrement de l'article dans la base de données
        Article savedArticle = articleRepository.save(article);

        // Récupération de l'article par son identifiant
        Optional<Article> retrievedArticle = articleService.getById(savedArticle.getId());

        // Vérification que l'article a été récupéré
        assertTrue(retrievedArticle.isPresent());
        assertEquals(savedArticle.getId(), retrievedArticle.get().getId());
        assertEquals("Nom de l'article", retrievedArticle.get().getDesignation());
        assertEquals(10, retrievedArticle.get().getQuantity());
        assertEquals(9.99f, retrievedArticle.get().getPrice());
    }

    @Test
    void createArticleTest() {
        // Création d'un nouvel article
        Article article = new Article();
        article.setDesignation("Nom de l'article");
        article.setQuantity(10);
        article.setPrice(9.99f);

        // Appel du service pour créer l'article
        Article createdArticle = articleService.create(article.getDesignation(), article.getQuantity(), article.getPrice());

        // Vérification des données de l'article créé
        assertNotNull(createdArticle.getId());
        assertEquals("Nom de l'article", createdArticle.getDesignation());
        assertEquals(10, createdArticle.getQuantity());
        assertEquals(9.99f, createdArticle.getPrice());
    }

    @Test
    void deleteArticleByIdTest() throws Exception {
        // Création d'un article
        Article article = new Article();
        article.setDesignation("Nom de l'article");
        article.setQuantity(10);
        article.setPrice(9.99f);

        // Enregistrement de l'article dans la base de données
        Article savedArticle = articleRepository.save(article);

        // Suppression de l'article par son identifiant
        articleService.deleteById(savedArticle.getId());

        // Vérification que l'article a été supprimé
        assertFalse(articleRepository.existsById(savedArticle.getId()));
    }
}
