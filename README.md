# TP : Webservice REST

Shop stock management

## Database

```bash
# Start database
docker compose up
```

## Database schema

![Database schema](./resources/schema.png)

## API

Server start on port 8080

```bash
# Server
http://localhost:8080

# Swagger
http://localhost:8080/swagger-ui/index.html
```

## Postman

Import postman collection

```bash
# Postman
./resources/postman.json
```
